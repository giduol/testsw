package br.com.testeSW.api;

import br.com.testeSW.api.testCases.excecoes.PersonagemInvalidoTest;
import br.com.testeSW.api.testCases.excecoes.VeiculoInvalidoTest;
import org.junit.runner.RunWith;
import org.junit.runners.Suite;
import org.junit.runners.Suite.SuiteClasses;

@RunWith(Suite.class)
@SuiteClasses({
    PersonagemInvalidoTest.class,
    VeiculoInvalidoTest.class
})

public class ExcecoesSuite {

}
