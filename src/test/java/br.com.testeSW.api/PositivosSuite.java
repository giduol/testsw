package br.com.testeSW.api;

import br.com.testeSW.api.testCases.positivos.Especie6Test;
import br.com.testeSW.api.testCases.positivos.Personagem1Test;
import br.com.testeSW.api.testCases.positivos.Planeta3Test;
import br.com.testeSW.api.testCases.positivos.Veiculo14Test;
import org.junit.runner.RunWith;
import org.junit.runners.Suite;
import org.junit.runners.Suite.SuiteClasses;

@RunWith(Suite.class)
@SuiteClasses({
    Personagem1Test.class,
    Planeta3Test.class,
    Veiculo14Test.class,
    Especie6Test.class
})

public class PositivosSuite {

}
