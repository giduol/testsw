package br.com.testeSW.api.testCases.HealthCheck;

import br.com.testeSW.api.BaseTest;
import org.junit.Test;
import static io.restassured.RestAssured.when;

public class HealthCheckTest extends BaseTest {

    @Test
    public void healthCheckActuator(){

        when().
            get("/").
        then().
            statusCode(200);
    }

}
