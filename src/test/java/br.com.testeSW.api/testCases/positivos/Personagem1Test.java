package br.com.testeSW.api.testCases.positivos;

import br.com.testeSW.api.BaseTest;
import org.junit.Test;

import java.util.ArrayList;
import java.util.List;

import org.junit.runner.RunWith;

import com.tngtech.java.junit.dataprovider.DataProvider;
import com.tngtech.java.junit.dataprovider.DataProviderRunner;
import com.tngtech.java.junit.dataprovider.UseDataProvider;

import static io.restassured.RestAssured.when;
import static org.hamcrest.CoreMatchers.is;

@RunWith(DataProviderRunner.class)
public class Personagem1Test extends BaseTest {

    @DataProvider
    public static Object[][] people1() {
        return new Object[][]{
                {"1", "Luke Skywalker", "172", "77", "blond", "fair", "blue", "19BBY", "male", "http://swapi.dev/api/planets/1/",
                 "http://swapi.dev/api/films/1/", "http://swapi.dev/api/films/2/", "http://swapi.dev/api/films/3/",
                 "http://swapi.dev/api/films/6/",  "http://swapi.dev/api/vehicles/14/", "http://swapi.dev/api/vehicles/30/",
                 "http://swapi.dev/api/starships/12/", "http://swapi.dev/api/starships/22/", "2014-12-09T13:50:51.644000Z",
                 "2014-12-20T21:17:56.891000Z", "http://swapi.dev/api/people/1/"}
        };
    }

    @Test
    @UseDataProvider("people1")
    public void validarDadosDePersonagens(String id, String name, String height, String mass, String hair_color, String skin_color,
                                          String eye_color, String birth_year, String gender, String homeworld,
                                          String film1, String film2, String film3, String film4,
                                          String vehicle1, String vehicle2, String starship1, String starship2,
                                          String created, String edited, String url){

        List filmes = new ArrayList();
        filmes.add(film1);
        filmes.add(film2);
        filmes.add(film3);
        filmes.add(film4);

        List especies = new ArrayList();

        List veiculos = new ArrayList();
        veiculos.add(vehicle1);
        veiculos.add(vehicle2);

        List naves = new ArrayList();
        naves.add(starship1);
        naves.add(starship2);

        when().
            get("/people/" + id).
        then().
            statusCode(200).
            body("name", is(name)).
            body("height", is(height)).
            body("mass", is(mass)).
            body("hair_color", is(hair_color)).
            body("skin_color", is(skin_color)).
            body("eye_color", is(eye_color)).
            body("birth_year", is(birth_year)).
            body("gender", is(gender)).
            body("homeworld", is(homeworld)).
            body("films", is(filmes)).
            body("species", is(especies)).
            body("vehicles", is(veiculos)).
            body("starships", is(naves)).
            body("created", is(created)).
            body("edited", is(edited)).
            body("url", is(url));
    }
}
