package br.com.testeSW.api.testCases.positivos;

import br.com.testeSW.api.BaseTest;
import br.com.testeSW.api.Dados;
import org.junit.Test;

import java.util.ArrayList;
import java.util.List;

import static io.restassured.RestAssured.when;
import static org.hamcrest.CoreMatchers.is;

public class Especie6Test extends BaseTest {

    @Test
    public void validarDadosDeEspecies(){

        List filmes = new ArrayList();
        filmes.add(Dados.lerDado("specie.films1"));
        filmes.add(Dados.lerDado("specie.films2"));
        filmes.add(Dados.lerDado("specie.films3"));
        filmes.add(Dados.lerDado("specie.films4"));
        filmes.add(Dados.lerDado("specie.films5"));

        List personagens = new ArrayList();
        personagens.add(Dados.lerDado("specie.people1"));

        when().
            get("/species/"+Dados.lerDado("specie.id")).
        then().
            statusCode(200).
            body("name", is(Dados.lerDado("specie.name"))).
            body("classification", is(Dados.lerDado("specie.classification"))).
            body("designation", is(Dados.lerDado("specie.designation"))).
            body("average_height", is(Dados.lerDado("specie.average_height"))).
            body("skin_colors", is(Dados.lerDado("specie.skin_colors"))).
            body("hair_colors", is(Dados.lerDado("specie.hair_colors"))).
            body("eye_colors", is(Dados.lerDado("specie.eye_colors"))).
            body("average_lifespan", is(Dados.lerDado("specie.average_lifespan"))).
            body("homeworld", is(Dados.lerDado("specie.homeworld"))).
            body("language", is(Dados.lerDado("specie.language"))).
            body("people", is(personagens)).
            body("films", is(filmes)).
            body("created", is(Dados.lerDado("specie.created"))).
            body("edited", is(Dados.lerDado("specie.edited"))).
            body("url", is(Dados.lerDado("specie.url")));
    }
}
