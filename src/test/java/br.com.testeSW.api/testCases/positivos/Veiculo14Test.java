package br.com.testeSW.api.testCases.positivos;

import br.com.testeSW.api.BaseTest;
import br.com.testeSW.api.Dados;
import org.junit.Test;
import java.util.ArrayList;
import java.util.List;
import static io.restassured.RestAssured.when;
import static org.hamcrest.CoreMatchers.is;

public class Veiculo14Test extends BaseTest {

    @Test
    public void validarDadosDeVeiculos(){

        List pilotos = new ArrayList();
        pilotos.add(Dados.lerDado("vehicle.pilots1"));
        pilotos.add(Dados.lerDado("vehicle.pilots2"));

        List filmes = new ArrayList();
        filmes.add(Dados.lerDado("vehicle.films1"));

        when().
            get("/vehicles/"+Dados.lerDado("vehicle.id")).
        then().
            statusCode(200).
            body("name", is(Dados.lerDado("vehicle.name"))).
            body("model", is(Dados.lerDado("vehicle.model"))).
            body("manufacturer", is(Dados.lerDado("vehicle.manufacturer"))).
            body("cost_in_credits", is(Dados.lerDado("vehicle.cost_in_credits"))).
            body("length", is(Dados.lerDado("vehicle.length"))).
            body("max_atmosphering_speed", is(Dados.lerDado("vehicle.max_atmosphering_speed"))).
            body("crew", is(Dados.lerDado("vehicle.crew"))).
            body("passengers", is(Dados.lerDado("vehicle.passengers"))).
            body("cargo_capacity", is(Dados.lerDado("vehicle.cargo_capacity"))).
            body("consumables", is(Dados.lerDado("vehicle.consumables"))).
            body("vehicle_class", is(Dados.lerDado("vehicle.vehicle_class"))).
            body("pilots", is(pilotos)).
            body("films", is(filmes)).
            body("created", is(Dados.lerDado("vehicle.created"))).
            body("edited", is(Dados.lerDado("vehicle.edited"))).
            body("url", is(Dados.lerDado("vehicle.url")));
    }
}
