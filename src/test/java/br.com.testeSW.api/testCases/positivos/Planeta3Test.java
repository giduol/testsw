package br.com.testeSW.api.testCases.positivos;

import br.com.testeSW.api.BaseTest;
import br.com.testeSW.api.Dados;
import org.junit.Test;
import java.util.ArrayList;
import java.util.List;
import static io.restassured.RestAssured.when;
import static org.hamcrest.CoreMatchers.is;

public class Planeta3Test extends BaseTest {

    @Test
    public void validarDadosDePlanetas(){

        List filmes = new ArrayList();
        filmes.add(Dados.lerDado("planet.films1"));

        when().
            get("/planets/"+ Dados.lerDado("planet.id")).
        then().
            statusCode(200).
            body("name", is(Dados.lerDado("planet.name"))).
            body("rotation_period", is(Dados.lerDado("planet.rotation_period"))).
            body("orbital_period", is(Dados.lerDado("planet.orbital_period"))).
            body("diameter", is(Dados.lerDado("planet.diameter"))).
            body("climate", is(Dados.lerDado("planet.climate"))).
            body("gravity", is(Dados.lerDado("planet.gravity"))).
            body("terrain", is(Dados.lerDado("planet.terrain"))).
            body("surface_water", is(Dados.lerDado("planet.surface_water"))).
            body("population", is(Dados.lerDado("planet.population"))).
            body("films", is(filmes)).
            body("created", is(Dados.lerDado("planet.created"))).
            body("edited", is(Dados.lerDado("planet.edited"))).
            body("url", is(Dados.lerDado("planet.url")));
    }
}
