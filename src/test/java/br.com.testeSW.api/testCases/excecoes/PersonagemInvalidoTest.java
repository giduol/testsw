package br.com.testeSW.api.testCases.excecoes;

import br.com.testeSW.api.BaseTest;
import org.junit.Test;

import static io.restassured.RestAssured.when;
import static org.hamcrest.CoreMatchers.is;

public class PersonagemInvalidoTest extends BaseTest {

    @Test
    public void validarDadosDePersonagens(){

        when().
            get("/people/17").
        then().
            statusCode(404).
            body("detail", is("Not found"));
    }
}
