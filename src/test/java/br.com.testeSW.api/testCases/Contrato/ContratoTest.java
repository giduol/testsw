package br.com.testeSW.api.testCases.Contrato;

import br.com.testeSW.api.BaseTest;
import org.junit.Test;
import org.junit.jupiter.api.DisplayName;

import java.io.File;
import static io.restassured.RestAssured.*;
import static io.restassured.module.jsv.JsonSchemaValidator.matchesJsonSchema;

public class ContratoTest extends BaseTest {

    @Test
    @DisplayName("Teste de Contrato")
    public void quebraContrato() {

        when().
            get("/").
        then().
            body(matchesJsonSchema(new File("src\\test\\resources\\json_schemas\\contrato.json")));
    }
}
