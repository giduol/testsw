package br.com.testeSW.api;

import br.com.testeSW.api.testCases.Contrato.ContratoTest;
import org.junit.runner.RunWith;
import org.junit.runners.Suite;
import org.junit.runners.Suite.SuiteClasses;

@RunWith(Suite.class)
@SuiteClasses({
        ContratoTest.class
})

public class ContratoSuite {

}
