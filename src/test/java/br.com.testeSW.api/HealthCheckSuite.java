package br.com.testeSW.api;

import br.com.testeSW.api.testCases.HealthCheck.HealthCheckTest;
import org.junit.runner.RunWith;
import org.junit.runners.Suite;
import org.junit.runners.Suite.SuiteClasses;

@RunWith(Suite.class)
@SuiteClasses({
    HealthCheckTest.class
})

public class HealthCheckSuite {

}
