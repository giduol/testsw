package br.com.testeSW.api;

import org.junit.BeforeClass;
import static io.restassured.RestAssured.*;

public abstract class BaseTest {

    @BeforeClass
    public static void preCondicao(){
        enableLoggingOfRequestAndResponseIfValidationFails();
        baseURI = Utils.lerDado("url.inicial");
        basePath = Utils.lerDado("path.inicial");
    }
}
