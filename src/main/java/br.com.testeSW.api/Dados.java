package br.com.testeSW.api;

import java.io.FileInputStream;
import java.util.Properties;

public class Dados {

    public static String lerDado(String nomePropriedade) {
        String valorDado = null;

        try {
            Properties dado = new Properties();
            dado.load(new FileInputStream("src/test/resources/dados/dados.txt"));

            valorDado = dado.getProperty(nomePropriedade);

        } catch (Exception e) {
            e.printStackTrace();
        }

        return valorDado;
    }
}
