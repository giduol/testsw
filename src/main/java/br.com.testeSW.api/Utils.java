package br.com.testeSW.api;

import java.io.FileInputStream;
import java.util.Properties;

public class Utils {

    public static String lerDado(String nomePropriedade) {
        String valorPropriedade = null;

        try {
            Properties properties = new Properties();
            properties.load(new FileInputStream("src/test/resources/config.properties"));

            valorPropriedade = properties.getProperty(nomePropriedade);

        } catch (Exception e) {
            e.printStackTrace();
        }

        return valorPropriedade;
    }
}
